<?php
namespace Kiozk\React\RpcServer;
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\LoopInterface;
use React\Http\Middleware\LimitConcurrentRequestsMiddleware;
use React\Http\Middleware\RequestBodyParserMiddleware;
use React\Http\Response;
use React\Http\StreamingServer;
use React\Socket\Server as SocketServer;
use Throwable;

class Server{
    /**
     * @var LoopInterface
     */
    private $_loop;
    private $_uri;

    /**
     * @var StreamingServer|null
     */
    private $_server;

    /**
     * @var SocketServer|null
     */
    private $_socket;

    /**
     * Массив зарегистрированных методов
     *
     * @var array
     */
    private $_methods = [];

    public function __construct($loop, $uri = null)
    {
        $this->_loop = $loop;

        if($uri === null){
            $this->_uri = $uri;
        }
        $this->init();
    }

    private function init(){

        $this->_socket = new SocketServer($this->_uri, $this->_loop);

        $this->_server = new StreamingServer([
            new LimitConcurrentRequestsMiddleware(100), // 100 concurrent buffering handlers
            //new RequestBodyBufferMiddleware(16 * 1024 * 1024), // 16 MiB
            new RequestBodyParserMiddleware(),
            [$this, 'onRequest']
        ]);
        $this->_server->listen($this->_socket);
    }

    /**
     * Обработка входящих запросов
     * @param ServerRequestInterface $request
     * @return Response
     */
    public function onRequest(ServerRequestInterface $request){
        $type = strtolower($request->getHeaderLine('Content-Type'));
        list ($type) = explode(';', $type);


        if($paredUrl = @parse_url($request->getUri())){
            if(isset($paredUrl['path']) && isset($this->_methods[$paredUrl['path']])){

                $availableMethods = array_unique(['OPTIONS'], array_keys($this->_methods[$paredUrl['path']]));
                $method = $request->getMethod();
                if($method === 'OPTIONS'){
                    return new Response(
                        200,
                        array(
                            'Content-Type' => 'application/json',
                            'Allow' => implode(', ', $availableMethods)
                        ),
                        json_encode([
                            'status' => 200,
                            'message' => 'Method not allowed, use: ' .  implode(', ', $availableMethods)
                        ])
                    );
                } else {
                    if(isset($this->_methods[$paredUrl['path']][$method])){
                        if(is_callable($this->_methods[$paredUrl['path']][$method])){
                            try {
                                return call_user_func($this->_methods[$paredUrl['path']][$method], $request);

                            }catch (Throwable $throwable){
                                return $this->httpError(500, $throwable->getMessage());

                            }
                        } else {
                            return $this->httpError(500, 'Method not callable');
                        }
                    } else {
                        return new Response(
                            405,
                            array(
                                'Content-Type' => 'application/json',
                                'Allow' => implode(', ', $availableMethods)
                            ),
                            json_encode([
                                'status' => 405,
                                'message' => 'Method not allowed, use: ' .  implode(', ', $availableMethods)
                            ])
                        );
                    }
                }

            } else {
                return $this->httpError(404, 'Method not found.');

            }
        } else {
            return $this->httpError(500, 'Parse url error.');
        }
    }

    private function httpError($status, $message){
        return new Response(
            $status,
            array(
                'Content-Type' => 'application/json'
            ),
            json_encode([
                'status' => $status,
                'message' => $message
            ])
        );
    }


    public function get($path, $callback){
        $this->_methods['GET'][$path] = $callback;
    }

    public function post($path, $callback){
        $this->_methods['POST'][$path] = $callback;
    }
    public function put($path, $callback){
        $this->_methods['PUT'][$path] = $callback;
    }


    public function delete($path, $callback){
        $this->_methods['DELETE'][$path] = $callback;
    }

    public function response($data, $status = 200){
        return new Response(
            $status,
            array(
                'Content-Type' => 'application/json'
            ),
            json_encode($data)
        );
    }
}